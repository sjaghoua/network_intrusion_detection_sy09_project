# %%
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from mlxtend.plotting import plot_decision_regions
from scipy import stats
from sklearn import svm
from sklearn.feature_selection import f_classif, SelectPercentile
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, confusion_matrix,roc_curve, auc
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler

import src.knn_validation as kv
from src.utils import standardize_features, add_decision_boundary, \
    plot_clustering
from src.pretraitement import testdata

data = pd.read_csv("./data/Train_data.csv", sep=",")
testdata = pd.read_csv("./data/Test_data.csv", sep=",")

col_dropped = ["is_host_login", "num_outbound_cmds"]
corr_matrix = data.corr().abs()
mask = np.triu(np.ones_like(corr_matrix, dtype=bool))
tri_df = corr_matrix.mask(mask)
to_drop = [col for col in tri_df.columns if any(tri_df[col] >= 0.94)]

data_reduced = data.drop(col_dropped+to_drop, axis=1)
testdata_reduced = testdata.drop(col_dropped+to_drop, axis=1)

categorical_columns = ['protocol_type', 'service', 'flag']
# Get the categorical values into a 2D numpy array
data_categorical_values = data_reduced[categorical_columns]
testdata_categorical_values = testdata_reduced[categorical_columns]
data_categorical_values.head()

unique_protocol = sorted(data_reduced.protocol_type.unique())
string1 = 'Protocol_type_'
unique_protocol2 = [string1 + x for x in unique_protocol]
# service
unique_service = sorted(data_reduced.service.unique())
string2 = 'service_'
unique_service2 = [string2 + x for x in unique_service]
# flag
unique_flag = sorted(data_reduced.flag.unique())
string3 = 'flag_'
unique_flag2 = [string3 + x for x in unique_flag]
# put together
dum_cols=unique_protocol2 + unique_service2 + unique_flag2
print(dum_cols)

# pareil pour le données du test
unique_service_test = sorted(testdata_reduced.service.unique())
unique_service2_test = [string2 + x for x in unique_service_test]
test_dum_cols = unique_protocol2 + unique_service2_test + unique_flag2

# Transformer les nominales features à entier en utilisant LabelEncoder()
data_categorical_values_int = data_categorical_values.apply(LabelEncoder().fit_transform)
print(data_categorical_values_int.head())
# test set
testdata_categorical_values_int = testdata_categorical_values.apply(LabelEncoder().fit_transform)

# One Hot Encoding
ohe = OneHotEncoder()
data_categorical_values_ohe = ohe.fit_transform(data_categorical_values_int)
data_dummy = pd.DataFrame(data_categorical_values_ohe.toarray(), columns=dum_cols)
# test set
testdata_categorical_values_ohe = ohe.fit_transform(testdata_categorical_values_int)
test_dummy = pd.DataFrame(testdata_categorical_values_ohe.toarray(), columns=test_dum_cols)

# Add one missing column from data to test data and also from test data to data
trainservice = data_reduced['service'].tolist()
testservice = testdata_reduced['service'].tolist()
difference = list(set(trainservice) - set(testservice))
string = 'service_'
difference = [string + x for x in difference]
difference

difference_test_data = list(set(testservice) - set(trainservice))
string = 'service_'
difference_test_data = [string + x for x in difference_test_data]
difference_test_data

for col in difference:
    test_dummy[col] = 0
test_dummy.shape

for col in difference_test_data:
    data_dummy[col] = 0
data_dummy.shape

# Ajouter encoded categorical dataframe à dataframe orignal
value_class = data_reduced['class']
X_data_reduced = data_reduced.drop('class', axis=1)
newdata = X_data_reduced.join(data_dummy)
newdata.insert(0,'class', value_class)
newdata.drop('flag', axis=1, inplace=True)
newdata.drop('protocol_type', axis=1, inplace=True)
newdata.drop('service', axis=1, inplace=True)
# test data
newdata_test = testdata_reduced.join(test_dummy)
newdata_test.drop('flag', axis=1, inplace=True)
newdata_test.drop('protocol_type', axis=1, inplace=True)
newdata_test.drop('service', axis=1, inplace=True)
print(newdata.shape)
print(newdata_test.shape)

X_data = newdata.drop(["class"], axis=1)
Y_data = newdata["class"]
X_train, X_test, Y_train, Y_test = train_test_split(X_data, Y_data, train_size=0.9, random_state=42)

print("shape of X_train set : {} rows, {} columns".format(X_train.shape[0], X_train.shape[1]))
print("shape of X_test set : {} rows, {} columns".format(X_test.shape[0], X_test.shape[1]))
print("length of Y_train {}, and Y_test {}".format(len(Y_train), len(Y_test)))

X_train_scaled, X_test_scaled = standardize_features(X_train, X_test)

print(X_train_scaled.std(axis=0))
print(X_test_scaled.std(axis=0))


# %% feature selection
def feature_selection_f_value(X, y):
    fvalue_selector = SelectPercentile(f_classif, percentile=10)
    X_selected = fvalue_selector.fit_transform(X, y)
    # Show results
    print('Original number of features:', X.shape[1])
    print('Reduced number of features:', X_selected.shape[1])
    print('features', fvalue_selector.get_support(indices=True))
    return X_selected

def plot_join_plot(df, feature, target):
    j = sns.jointplot(feature, target, data = df, kind = 'reg')
    j.annotate(stats.pearsonr)
    return plt.show()

labelEncoder = LabelEncoder()
Y_train = pd.Series(labelEncoder.fit_transform(Y_train.astype('str')), name="class")
Y_test = pd.Series(labelEncoder.transform(Y_test.astype('str')), name="class")
X_selected = feature_selection_f_value(X_train_scaled, Y_train)
train_df = pd.concat([X_train_scaled,Y_train], axis=1)
plot_join_plot(train_df,"dst_bytes", "class")

plt.show()
X_train_scaled.describe()

# %% SVM

def plot_grid_search(cv_results):
    # Get Test Scores Mean and std for each grid search
    import seaborn as sns
    scores_mean = cv_results['mean_test_score']
    c_value = np.array([0.001, 0.01, 0.1, 1, 10, 100])

    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    df = pd.DataFrame({"Valeur C":c_value, "Mean Test Score":scores_mean})
    lp=sns.lineplot(x="Valeur C", y="Mean Test Score", data=df)
    lp.set(xscale="log")
    plt.show()

clf = svm.SVC(kernel='linear') # Linear Kernel
clf.fit(X_train_scaled, Y_train)
plot_grid_search(clf.cv_results_)
##############################
tuned_parameters = [{'C': [0.001, 0.01, 0.1, 1, 10,100]}]
clf = GridSearchCV(svm.SVC(kernel='linear'), tuned_parameters, cv=10, scoring='accuracy')
##############################
clf.fit(X_train_scaled, Y_train)
clf.cv_results_
clf.best_params_

y_pred = clf.predict(X_test_scaled)
Y_test.value_counts()
print("Accuracy:",accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:",f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))

tuned_parameters = [{'C': [0.001, 0.01, 0.1, 1, 10,100],
                     'gamma': [1.e-03, 1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03]}]
clf_rbf = GridSearchCV(svm.SVC(kernel='rbf'), tuned_parameters, cv=10, scoring='accuracy')
clf_rbf.best_params_
C= [0.001, 0.01, 0.1, 1, 10,100]
Gammas = [1.e-03, 1.e-02, 1.e-01, 1.e+00, 1.e+01, 1.e+02, 1.e+03]
scores = clf_rbf.cv_results_['mean_test_score']
scores = np.array(scores).reshape(len(C), len(Gammas))
for ind, i in enumerate(C):
    plt.plot(Gammas, scores[ind], label='C: ' + str(i))
plt.legend(loc="upper right")
plt.xscale("log")
plt.xlabel('Gamma')
plt.ylabel('Mean Test Score')
plt.show()
clf_rbf = svm.SVC(probability=True, kernel='rbf', C=10)
clf_rbf.fit(X_train_scaled, Y_train)


y_pred = clf_rbf.predict(X_test_scaled)
print("Accuracy score:", accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:",f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))

svc = svm.SVC(probability=True, kernel='rbf', C=100, gamma=0.1)
svc.fit(X_train_scaled, Y_train)
y_prob = svc.predict_proba(X_test_scaled)
labelEncoder = LabelEncoder()
Y_test = pd.Series(labelEncoder.fit_transform(Y_test.astype('str')), name="class")
false_positive_rate, true_positive_rate, thresholds = roc_curve(Y_test, y_prob[:,1])
roc_auc = auc(false_positive_rate, true_positive_rate)
print("Area under the ROC curve : %f" % roc_auc)
plt.plot(false_positive_rate, true_positive_rate, label='ROC curve (area = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()
# %% knn
n_neighbors_list = np.unique(np.round(np.geomspace(1, 500, 100)).astype(int))

gen = kv.knn_simple_validation(X_train_scaled, Y_train, X_test_scaled, Y_test, n_neighbors_list)
df = pd.DataFrame(gen, columns=["# neighbors", "accuracy", "degrés de liberté"])
sp = sns.lineplot(x="degrés de liberté", y="accuracy", data=df)
sp.set(xscale="log")
plt.show()

Kopt = df.loc[df["accuracy"].idxmax(), "# neighbors"]
scaler = StandardScaler(copy=True, with_mean=True, with_std=True)
X_data_scaled = scaler.fit_transform(X_data)
cls = KNeighborsClassifier(n_neighbors = Kopt)
cls.fit(X_data, Y_data)
plot_clustering(X_data, Y_data)
add_decision_boundary(cls)
plt.show()

######################################
# %%
n_neighbors_list = np.unique(np.round(np.geomspace(1, 100, 50)).astype(int))
X_knn = X_train_scaled.iloc[:,0:30]
knn_col=X_knn.columns
X_knn_test = X_test_scaled.iloc[:,0:30]
param_grid = {"n_neighbors": n_neighbors_list}
cls = KNeighborsClassifier()
search = GridSearchCV(cls, param_grid, scoring="accuracy", cv=10)
search.fit(X_knn, Y_train)
search.best_params_
search.grid_scores_

df =pd.DataFrame(
    (
        dict(n_neighbors=d["n_neighbors"], error=e, std=s) for d, e, s in zip(
        search.cv_results_["params"], search.cv_results_["mean_test_score"], search.cv_results_["std_test_score"],
        ) )
    )
n = 9/10 * len(Y_train)
plt.errorbar(n/df["n_neighbors"], df["error"], yerr=df["std"])
plt.xlabel("degrés de liberté")
plt.ylabel("accuracy")
plt.xscale("log")
plt.show()

X_knn_test_all = testdata[knn_col]
X_knn, X_knn_test_all = standardize_features(X_train[knn_col], X_knn_test_all)

Y_knn_test_all = newdata_test[["class"]]
Y_knn_test_all = Y_knn_test_all['class'].replace('attack','anomaly')
y_pred = search.predict(X_knn_test_all)



print("Accuracy:",accuracy_score(Y_knn_test_all, y_pred))
print("Precision score:", precision_score(Y_knn_test_all, y_pred, average="macro"))
print("Recall score:", recall_score(Y_knn_test_all, y_pred, average="macro"))
print("F1-score:",f1_score(Y_knn_test_all, y_pred, average="macro"))
print(confusion_matrix(Y_knn_test_all, y_pred))
################## small test data
# Accuracy: 0.9908730158730159
# Precision score: 0.990973630831643
# Recall score: 0.9906703830986807
# F1-score: 0.9908180360007128
# [[1151   14]
# #  [   9 1346]]
################## big test data
# Accuracy: 0.9211456166466793
# Precision score: 0.8563006273928742
# Recall score: 0.9445717139824542
# F1-score: 0.8889976926283161
# [[226943  23493]
#  [  1033  59560]]
Y_knn_test_all.value_counts()
# Out[57]:
# anomaly    250436
# normal      60593

# %%
n_neighbors_list = np.unique(np.round(np.geomspace(1, 100, 50)).astype(int))
train_x = X_train_scaled.iloc[:,0:30]
test_x = X_test_scaled.iloc[:,0:30]
train_accuracy = np.empty(len(n_neighbors_list))
test_accuracy = np.empty(len(n_neighbors_list))
for i, k in enumerate(n_neighbors_list):
    # We instantiate the classifier
    knn = KNeighborsClassifier(n_neighbors=k)
    # Fit the classifier to the training data
    knn.fit(train_x, Y_train)

    # Compute accuracy on the training set
    train_accuracy[i] = knn.score(train_x, Y_train)

    # Compute accuracy on the testing set
    test_accuracy[i] = knn.score(test_x, Y_test)

# -------------------------------------------------------------------
plt.title('k-NN: Varying Number of Neighbors')
plt.plot(n_neighbors_list, test_accuracy, label = 'Testing Accuracy')
plt.plot(n_neighbors_list, train_accuracy, label = 'Training Accuracy')
plt.legend()
plt.xlabel('Number of Neighbors')
plt.ylabel('Accuracy')
plt.show()

knn_best = KNeighborsClassifier(n_neighbors=1)
knn_best.fit(train_x, Y_train)
y_prob = knn_best.predict_proba(test_x)
false_positive_rate, true_positive_rate, thresholds = roc_curve(Y_test, y_prob[:,1])
roc_auc = auc(false_positive_rate, true_positive_rate)

print("Area under the ROC curve : %f" % roc_auc)
plt.plot(false_positive_rate, true_positive_rate, label='ROC curve (area = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()