# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import seaborn as sns
from src.pretraitement import data, testdata
from sklearn.ensemble import RandomForestClassifier
from src.pretraitement_fonc import seperate_train_test, one_hot_encoding
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, confusion_matrix, \
    roc_curve, auc,plot_confusion_matrix
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

# %%
df_data = data.drop(['is_host_login', 'num_outbound_cmds','name_of_attack','type_of_attack'], axis = 1)
df_test = testdata.drop(['is_host_login', 'num_outbound_cmds','name_of_attack'], axis = 1)

new_df_data, new_df_test = one_hot_encoding(df_data,df_test)
rf_data = new_df_data[new_df_data.columns.drop(list(new_df_data.filter(regex='service_')))]
rf_test = new_df_test[new_df_test.columns.drop(list(new_df_test.filter(regex='service_')))]

# X_train, Y_train, X_test, Y_test = seperate_train_test(rf_data)
X_train = rf_data.drop(['class'],axis = 1)
Y_train = rf_data['class']
X_test = rf_test.drop(['class'],axis = 1)
Y_test = rf_test['class']
model_rf = RandomForestClassifier(n_estimators = 40,
                                  criterion = 'entropy',
                                  random_state = 42)
model_rf.fit(X_train, Y_train)

#预测测试集中的数据
y_pred = model_rf.predict(X_test)
y_prob =  model_rf.predict_proba(X_test)
print("Accuracy:",accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:",f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))
plot_confusion_matrix(model_rf, X_test, Y_test, normalize= 'true')
plt.show()

# Accuracy: 0.9266692173398622
# Precision score: 0.8634310997396324
# Recall score: 0.9525367956656341
# F1-score: 0.8966381466463156
# [[227936  22500]
#  [   308  60285]]

# labelEncoder = LabelEncoder()
# Y_test = pd.Series(labelEncoder.fit_transform(Y_test.astype('str')), name="class")
# y_pred = pd.Series(labelEncoder.transform(y_pred.astype('str')), name="class")
# false_positive_rate, true_positive_rate, thresholds = roc_curve(Y_test, y_prob[:,1])
# roc_auc = auc(false_positive_rate, true_positive_rate)

# print("Area under the ROC curve : %f" % roc_auc)
# plt.plot(false_positive_rate, true_positive_rate, label='ROC curve (area = %0.2f)' % roc_auc)
# plt.plot([0, 1], [0, 1], 'k--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.0])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('Receiver operating characteristic example')
# plt.legend(loc="lower right")
# plt.show()

########### 以下不看！！！！！！！！！！！！！！
