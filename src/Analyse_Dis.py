# %% md

# LDA and QDA

# %%
from src.pretraitement_fonc import seperate_train_test, one_hot_encoding, standardize_features
import os

os.chdir("..")
# from src.pretraitement import X_train,X_train_scaled,Y_train,X_test,X_test_scaled,Y_test
from src.pretraitement import data, testdata
# %%

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB

# %%

import numpy as np
import pandas as pd


# %%

def training_model(model, data, labels):
    clf = model()
    clf.fit(data, labels)
    return clf


def ratio_precision(Y_predicted, Y):
    if not (Y_predicted.shape == Y.shape):
        raise ValueError("Y_predicted and Y did not have the same shape")

    return (Y_predicted == Y).sum() / Y.shape[0]


def validation_model(clf, data_test, labels_test):
    Y_predicted = clf.predict(data_test)
    return ratio_precision(Y_predicted, labels_test), Y_predicted


def validation_model_intra_class(clf, data_test, labels_test):
    labels = labels_test.unique()

    for l in labels:
        mask = (labels_test == l)
        print(l, validation_model(clf, data_test[mask], labels_test[mask])[0])


# %%

## LDA
df_data = data.drop(['is_host_login', 'num_outbound_cmds', 'name_of_attack', 'type_of_attack'], axis=1)
df_test = testdata.drop(['is_host_login', 'num_outbound_cmds', 'name_of_attack'], axis=1)

new_df_data, new_df_test = one_hot_encoding(df_data, df_test)
rf_data = new_df_data[new_df_data.columns.drop(list(new_df_data.filter(regex='service_')))]
rf_test = new_df_test[new_df_test.columns.drop(list(new_df_test.filter(regex='service_')))]

# X_train, Y_train, X_test, Y_test = seperate_train_test(rf_data)
X_train = rf_data.drop(['class'], axis=1)
Y_train = rf_data['class']
X_test = rf_test.drop(['class'], axis=1)
Y_test = rf_test['class']
X_train_scaled, X_test_scaled = standardize_features(X_train, X_test)
# %%

clf_lda = training_model(LinearDiscriminantAnalysis, X_train_scaled, Y_train)

# %%
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, confusion_matrix, \
    roc_curve, auc, plot_confusion_matrix

# validation_model(clf_lda,X_test,Y_test)[0]
y_pred = clf_lda.predict(X_test_scaled)
print("Accuracy:", accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:", f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))
# %%

validation_model_intra_class(clf_lda, X_test, Y_test)

# %%

Y_test.unique()

# %%

mask_attack = (Y_test == "attack")
mask_normal = (Y_test == "normal")

# %%

Y_test[mask_attack].shape[0] / Y_test.shape[0]

# %% md

## QDA

# %%

clf_qda = training_model(QuadraticDiscriminantAnalysis, X_train_scaled, Y_train)

# %%

# validation_model(clf_qda,X_test,Y_test)[0]
y_pred = clf_qda.predict(X_test_scaled)
print("Accuracy:", accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:", f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))
# %%

validation_model_intra_class(clf_qda, X_test, Y_test)

# %%

clf_qda2 = training_model(QuadraticDiscriminantAnalysis, X_train_scaled, Y_train)

# %%

validation_model(clf_qda2, X_test_scaled, Y_test)[0]

# %%

corr = (X_train_scaled.cov()).to_numpy()

# %%

corr[(corr >= 0.5) * (corr < 1)]

# %% md

## Gaussian naive

# %%

clf_gnb = training_model(GaussianNB, X_train_scaled, Y_train)

# %%
y_pred = clf_gnb.predict(X_test_scaled)
print("Accuracy:", accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:", f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))
# validation_model(clf_gnb,X_test,Y_test)[0]

# %%

validation_model_intra_class(clf_gnb, X_test, Y_test)

