import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder,StandardScaler
from mlxtend.plotting import plot_pca_correlation_graph
from src.utils import plotPerColumnDistribution
from src.pretraitement import X_train_scaled, X_train, Y_train, X_test, X_test_scaled, Y_test,\
    newdata, newdata_test, data_categorical_values


newdata['class']=pd.DataFrame(newdata['class']).apply(LabelEncoder().fit_transform)

newdata_quan = newdata.iloc[:,1:28]
scaler = StandardScaler(copy=True, with_mean=True, with_std=True)
newdata_quan.iloc[:,1:27] = scaler.fit_transform(newdata_quan.iloc[:,1:27])

df = pd.concat([newdata_quan, data_categorical_values], axis=1)
df.columns

plotPerColumnDistribution(df[[
    'protocol_type',
    'service',
    'flag',
    'duration',
    'src_bytes', 'dst_bytes',
    'land', 'wrong_fragment',
    'urgent', 'hot', 'num_failed_logins', 'logged_in', 'root_shell',
    'su_attempted', 'num_root', 'num_file_creations', 'num_shells',
    'num_access_files', 'is_guest_login', 'same_srv_rate', 'diff_srv_rate',
    'srv_diff_host_rate', 'dst_host_count', 'dst_host_same_srv_rate',
    'dst_host_diff_srv_rate', 'dst_host_same_src_port_rate',
    'dst_host_srv_diff_host_rate', 'dst_host_srv_serror_rate',
    'dst_host_srv_rerror_rate'
]], nGraphShown=30, nGraphPerRow=3)

fig,axes =plt.subplots(6,2, figsize=(5, 10)) # 3 columns each containing 10 figures, total 30 features
col_const=['class','land', 'wrong_fragment','urgent', 'hot','num_failed_logins', 'root_shell',
           'su_attempted', 'num_root','num_file_creations', 'num_shells','num_access_files','is_guest_login']
newdata_quan = newdata[col_const]
normal=newdata_quan[newdata_quan['class']==1]
attack=newdata_quan[newdata_quan['class']==0]
ax=axes.ravel()# flat axes with numpy ravel

for i in range(1, 13):
  _,bins=np.histogram(newdata_quan.iloc[:,i],bins=60)
  ax[i-1].hist(attack.iloc[:,i],bins=bins,color='r',alpha=.5)# red color for normal class
  ax[i-1].hist(normal.iloc[:,i],bins=bins,color='g',alpha=0.3)# alpha is  for transparency in the overlapped region
  ax[i-1].set_title(newdata_quan.columns[i],fontsize=9)
  ax[i-1].axes.get_xaxis().set_visible(True) # the x-axis co-ordinates are not so useful, as we just want to look how well separated the histograms are
  ax[i-1].axes.get_yaxis().set_visible(True)
  ax[i-1].legend(['attack', 'normal'], loc='best', fontsize=8)
  ax[i-1].set_yticks(())

plt.tight_layout()# let's make good plots
plt.savefig('histogramme.png')
plt.show()


cls = PCA(n_components=14)
X_train_scaled.iloc[:,0:26].head()
X = X_train_scaled.iloc[:,0:26].drop(['land', 'wrong_fragment','urgent', 'hot',
                                      'num_failed_logins', 'root_shell',
                                      'su_attempted', 'num_root','num_file_creations',
                                      'num_shells','num_access_files','is_guest_login'], axis=1)

col=X.columns
pcs_X_train = cls.fit_transform(X)
cls.explained_variance_ratio_
df_X_train = pd.DataFrame(pcs_X_train, columns=[f"PC{i}" for i in range(0, 14)])
Y= (Y_train.replace({'normal': 'blue','attack':'red'}, regex=True))

def myplot(score,coeff,labels=None):
    xs = score[:,0]
    ys = score[:,1]
    n = coeff.shape[0]
    scalex = 1.0/(xs.max() - xs.min())
    scaley = 1.0/(ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, c=Y, s=1)
    for i in range(n):
        plt.arrow(0, 0, coeff[i,0]/1.5, coeff[i,1]/1.5,color = 'green',alpha = 0.5)
        if labels is None:
            plt.text(coeff[i,0]* 1.15/1.5, coeff[i,1] * 1.15/1.5, "Var"+str(i+1), color = 'g', ha = 'center', va = 'center')
            print(labels[i])
        else:
            plt.text(coeff[i,0]* 1.15/1.5, coeff[i,1] * 1.15/1.5, labels[i], color = 'g', ha = 'center', va = 'center')
    plt.xlim(-0.5,1)
    plt.ylim(-0.4,1)
    plt.xlabel("PC{}({})".format(1,"26.8%"))
    plt.ylabel("PC{}({})".format(2,"16.9%"))
    plt.grid()

#Call the function. Use only the 2 PCs.
myplot(pcs_X_train[:,0:2],np.transpose(cls.components_[0:2, :]),col)
plt.show()

sns.scatterplot(x="PC1", y="PC2", hue=Y_train, data=df_X_train, s=10)
plt.show()
plt.plot(np.cumsum(cls.explained_variance_ratio_), linestyle='--', color='b')
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance');
plt.axhline(y=0.95, color='r', linestyle='-')
plt.xticks(np.arange(0, 15, step=1))
plt.text(0.5, 0.9, '95% cut-off threshold', color = 'red', fontsize=14)
plt.show()
