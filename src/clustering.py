import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans


from src.pretraitement import data_categorical_values, testdata_categorical_values
data_categorical_values.head()

value_service = data_categorical_values.iloc[:,1:2]

kmeans = KMeans(5)
kmeans.fit(value_service)

identified_clusters = kmeans.fit_predict(value_service)
identified_clusters

value_service['cluster'] = identified_clusters
value_service

plt.scatter(value_service)

