# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm
from sklearn.decomposition import PCA
from mlxtend.plotting import plot_decision_regions
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix

from src.pretraitement import data, testdata
from src.pretraitement_fonc import one_hot_encoding, standardize_features
# from src.pretraitement import X_train_scaled, X_train, Y_train, X_test, X_test_scaled, Y_test

# %%
# SVM
df_data = data.drop(['is_host_login', 'num_outbound_cmds','name_of_attack','type_of_attack'], axis = 1)
df_test = testdata.drop(['is_host_login', 'num_outbound_cmds','name_of_attack'], axis = 1)

new_df_data, new_df_test = one_hot_encoding(df_data,df_test)
rf_data = new_df_data[new_df_data.columns.drop(list(new_df_data.filter(regex='service_')))]
rf_test = new_df_test[new_df_test.columns.drop(list(new_df_test.filter(regex='service_')))]

# X_train, Y_train, X_test, Y_test = seperate_train_test(rf_data)
X_train = rf_data.drop(['class'],axis = 1)
Y_train = rf_data['class']
X_test = rf_test.drop(['class'],axis = 1)
Y_test = rf_test['class']
X_train_scaled, X_test_scaled = standardize_features(X_train,X_test)

clf = svm.SVC(probability=True, kernel='linear', C=10) # Linear Kernel
clf.fit(X_train_scaled, Y_train)
y_pred = clf.predict(X_test_scaled)
Y_test.value_counts()
print("Accuracy:",accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:",f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))


clf_rbf = svm.SVC(probability=True, kernel='rbf', C=100, gamma=0.1)
clf_rbf.fit(X_train_scaled, Y_train)
y_pred = clf_rbf.predict(X_test_scaled)
print("Accuracy score:", accuracy_score(Y_test, y_pred))
print("Precision score:", precision_score(Y_test, y_pred, average="macro"))
print("Recall score:", recall_score(Y_test, y_pred, average="macro"))
print("F1-score:",f1_score(Y_test, y_pred, average="macro"))
print(confusion_matrix(Y_test, y_pred))

# X_test_scaled[['wrong_fragment','urgent']].ax_matrix

# plot_decision_regions(X_test_scaled[['wrong_fragment','urgent']].as_matrix(), Y_test.astype(np.integer).as_matrix(), clf=clf, legend=2)
