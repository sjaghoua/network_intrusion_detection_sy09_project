# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

import warnings
warnings.filterwarnings('ignore')


from src.Analyse_Dis import clf_lda,clf_qda,clf_gnb, X_train_scaled, X_test_scaled, Y_train, Y_test
from sklearn.preprocessing import LabelEncoder
from src.RandomForest import model_rf
from src.Logistic_reg import clf_lr
from src.svm import clf_rbf, clf
from src.pretraitement_reduit import knn_best
# %%
labelEncoder = LabelEncoder()
Y_test = pd.Series(labelEncoder.fit_transform(Y_test.astype('str')), name="class")

from sklearn.metrics import roc_curve, roc_auc_score
# Instantiate the classfiers and make a list
classifiers = [clf_lda, clf_qda, clf_gnb, clf_lr, model_rf, clf, clf_rbf, knn_best]

# Define a result table as a DataFrame
result_table = pd.DataFrame(columns=['classifiers', 'fpr', 'tpr', 'auc'])

# Train the models and record the results
for cls in classifiers:
    model = cls.fit(X_train_scaled, Y_train)
    yproba = model.predict_proba(X_test_scaled)[::, 1]

    fpr, tpr, _ = roc_curve(Y_test, yproba)
    auc = roc_auc_score(Y_test, yproba)

    result_table = result_table.append({'classifiers': cls.__class__.__name__,
                                        'fpr': fpr,
                                        'tpr': tpr,
                                        'auc': auc}, ignore_index=True)

# Set name of the classifiers as index labels
result_table.set_index('classifiers', inplace=True)

# %%
fig = plt.figure(figsize=(8, 6))

for i in result_table.index:
    plt.plot(result_table.loc[i]['fpr'],
             result_table.loc[i]['tpr'],
             label="{}, AUC={:.3f}".format(i, result_table.loc[i]['auc']))

plt.plot([0, 1], [0, 1], color='orange', linestyle='--')

plt.xticks(np.arange(0.0, 1.1, step=0.1))
plt.xlabel("Flase Positive Rate", fontsize=15)

plt.yticks(np.arange(0.0, 1.1, step=0.1))
plt.ylabel("True Positive Rate", fontsize=15)

plt.title('ROC Curve Analysis', fontweight='bold', fontsize=15)
plt.legend(prop={'size': 13}, loc='lower right')

plt.show()